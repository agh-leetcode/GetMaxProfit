public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.runTheApp();
    }

    private void runTheApp() {
        int prices [] = new int[]{7,1,5,3,6,4};
        int prices2 [] = new int[]{1,2,5,6,4,5};
        int maxProfit = maxProfit(prices2);
        System.out.println(maxProfit);
    }

    //algorithm to find the maximum profit, the i-th element is the price of a given stock on day i.
    private int maxProfit(int[] prices) {
        int maxProfit=0;
        if (prices==null||prices.length<=1){
            return 0;
        }
        for (int i = 1; i < prices.length; i++) {
            if (prices[i]>prices[i-1]){
                maxProfit += prices[i] - prices[i-1];
            }
        }
        return maxProfit;
    }
}
